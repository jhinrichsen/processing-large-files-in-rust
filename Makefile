# If possible, all command options are optimized to take existing files into
# account, and only do something if anything changed at all.

DATADIR := input
DATA := itcont.txt
DATA_SMALL := itcont-10000.txt

.PHONY: data
data: $(DATADIR)/$(DATA) $(DATADIR)/$(DATA_SMALL)

.PHONY: clean
clean:
	rm -rf builds got.txt indiv18.zip $(DATA) reference p? performance-patterns want.txt
	rm -rf $(DATADIR)

$(DATADIR):
	mkdir $(DATADIR)

indiv18.zip:
	wget -N -nv https://www.fec.gov/files/bulk-downloads/2018/indiv18.zip
	
$(DATADIR)/$(DATA): indiv18.zip
	unzip -d $(DATADIR) -uo indiv18.zip $(DATA)
	
#$(DATADIR)/$(DATA_SMALL): $(DATADIR)/$(DATA)
#	head -n 10000 $< >$@
$(DATADIR)/$(DATA_SMALL):
	head -n 10000 $< >$@

.PHONY: test
test: $(DATADIR)/$(DATA_SMALL)
	cargo test