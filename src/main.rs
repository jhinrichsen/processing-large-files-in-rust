use std::collections::HashMap;
use std::io::{self, Read};
use std::sync::mpsc;
use std::thread;

fn main() -> io::Result<()> {
    println!("starting...");
    // TODO maybe add some optimization here, such as buffering e.a.
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;
    // let mut output = process(&buffer);
    let mut output = process_parallel(&buffer);
    output.sort();
    for line in output {
        println!("{}", line);
    }
    Ok(())
}

fn name(line: &str) -> &str {
    let mut f = line.split("|");
    f.next();
    f.next();
    f.next();
    f.next();
    f.next();
    f.next();
    f.next();
    let s = f.next().unwrap();
    return s;
}

fn count_lines(data: &str) -> usize {
    data.lines().count()
}

fn lines(data: &str) -> Vec<String> {
    vec![format!("Total file line count: {}", count_lines(data))]
}

// Names at indices 0, 432, and 43243
fn names(data: &str) -> Vec<String> {
    let mut ss = Vec::new();
    for (i, line) in data.lines().enumerate() {
        if i == 0 || i == 432 || i == 43243 {
            ss.push(format!("Name: {} at index: {}", name(line), i));
        }
        // take a shortcut
        if i == 43243 {
            break;
        }
    }
    ss
}

fn quartal(line: &str) -> String {
    let mut f = line.split("|");
    f.next();
    f.next();
    f.next();
    f.next();
    let field = f.next().unwrap();
    let year = &field[0..4];
    let month = &field[4..6];
    format!("{}-{}", month, year)
}

fn quartals(data: &str) -> Vec<String> {
    let mut counts = HashMap::new();
    for line in data.lines() {
        let key = quartal(line);
        let cnt = counts.entry(key).or_insert(0);
        *cnt += 1;
    }
    let mut ss = Vec::new();
    for (q, n) in counts {
        ss.push(format!(
            "Donations per month and year: {} and donation count: {}",
            q, n
        ));
    }
    ss
}

const DATA: &str = r#"C00629618|N|TER|P|201701230300133512|15C|IND|PEREZ, JOHN A|LOS ANGELES|CA|90017|PRINCIPAL|DOUBLE NICKEL ADVISORS|01032017|40|H6CA34245|SA01251735122|1141239|||2012520171368850783
C00177436|N|M2|P|201702039042410893|15|IND|WATJEN, THOMAS R.|KEY LARGO|FL|330375267|UNUM|CHAIRMAN OF THE BOARD|01042017|5000||40373239|1147350|||4020820171370029334
C00177436|N|M2|P|201702039042410893|15|IND|SABOURIN, JAMES|LOOKOUT MOUNTAIN|GA|307502818|UNUM|SVP, CORPORATE COMMUNICATIONS|01312017|230||PR1890575345050|1147350||P/R DEDUCTION ($115.00 BI-WEEKLY)|4020820171370029335
C00177436|N|M2|P|201702039042410893|15|IND|MAKER, SCOTT T.|NORTH YARMOUTH|ME|040976952|UNUM|SVP, DEPUTY GENERAL COUNSEL, BUSINESS|01312017|384||PR2260663445050|1147350||P/R DEDUCTION ($192.00 BI-WEEKLY)|4020820171370029336
C00177436|N|M2|P|201702039042410894|15|IND|DEEHAN, WILLIAM N|ALPHARETTA|GA|300047357|UNUM|SVP, SALES, CL|01312017|384||PR2283873845050|1147350||P/R DEDUCTION ($192.00 BI-WEEKLY)|4020820171370029337
C00177436|N|M2|P|201702039042410894|15|IND|PYNE, CHRISTOPHER W|COHASSET|MA|020251500|UNUM|SVP, SALES & CLIENT MGMT|01312017|230||PR2283874245050|1147350||P/R DEDUCTION ($115.00 BI-WEEKLY)|4020820171370029338
C00177436|N|M2|P|201702039042410894|15|IND|FOLEY, JOSEPH|FALMOUTH|ME|041051935|UNUM|SVP, CORP MKTG & PUBLIC RELAT.|01312017|384||PR2283904845050|1147350||P/R DEDUCTION ($192.00 BI-WEEKLY)|4020820171370029339
C00177436|N|M2|P|201702039042410895|15|IND|MCGARRY, JOHN|HOLLIS CENTER|ME|040424132|UNUM|EVP & CFO|01312017|384||PR2283905045050|1147350||P/R DEDUCTION ($192.00 BI-WEEKLY)|4020820171370029340
C00177436|N|M2|P|201702039042410895|15|IND|SIMONDS, MICHAEL Q|FALMOUTH|ME|041051972|UNUM|PRESIDENT & CEO, UNUM US|01312017|384||PR2283905145050|1147350||P/R DEDUCTION ($192.00 BI-WEEKLY)|4020820171370029341
C00177436|N|M2|P|201702039042410895|15|IND|JEROME, CHRISTOPHER|FALMOUTH|ME|041051896|UNUM|EVP, GLOBAL SERVICES|01312017|384||PR2283905245050|1147350||P/R DEDUCTION ($192.00 BI-WEEKLY)|4020820171370029342"#;

// #[cfg(test)]
// mod tests {
#[test]
fn number_of_lines() {
    assert_eq!(count_lines(DATA), 10)
}

#[test]
fn test_name() {
    assert_eq!(
        name("C00629618|N|TER|P|201701230300133512|15C|IND|PEREZ, JOHN A|LOS ANGELES|CA|90017|"),
        "PEREZ, JOHN A"
    );
}

#[test]
fn test_lines() {
    assert_eq!(lines(DATA)[0], "Total file line count: 10")
}

#[test]
fn test_names() {
    assert_eq!(names(DATA)[0], "Name: PEREZ, JOHN A at index: 0");
}

#[test]
fn test_quartal() {
    assert_eq!(
        quartal("C00629618|N|TER|P|201701230300133512|15C|IND|PEREZ, JOHN A|LOS ANGELES|CA|90017|"),
        "01-2017"
    );
}

#[test]
fn test_quartals() {
    // HashMap has no consistent order
    let mut qs = quartals(DATA);
    qs.sort();
    assert_eq!(
        qs[0],
        "Donations per month and year: 01-2017 and donation count: 1"
    );
    assert_eq!(
        qs[1],
        "Donations per month and year: 02-2017 and donation count: 9"
    );
}
// }

pub fn process(data: &str) -> Vec<String> {
    // All functions get their own unique “strong” type, so we need to
    // explicitly specify the weak type:
    let fns: Vec<fn(&str) -> Vec<String>> = vec![most_common_first_name, lines, names, quartals];
    let mut output = Vec::new();
    for f in fns {
        let o = f(data);
        output.extend(o);
    }
    output
}

pub fn process_parallel(data: &str) -> Vec<String> {
    // All functions get their own unique “strong” type, so we need to
    // explicitly specify the weak type:
    let fns: Vec<fn(&str) -> Vec<String>> = vec![most_common_first_name, lines, names, quartals];
    let (sender, receiver) = mpsc::channel();
    let mut children = vec![];
    for f in fns {
        let snd = sender.clone();
        children.push(thread::spawn(move || {
            let o = f(data);
            for s in o {
                let res = snd.send(s);
            }
        }));
    }
    for child in children {
        // Wait for the thread to finish. Returns a result.
        let _ = child.join();
    }
    let mut output = Vec::new();
    for s in receiver {
        output.push(s);
    }
    output
}

#[test]
fn test_all() {
    let mut output = process(DATA);
    output.sort();
    for line in output {
        println!("{}", line);
    }
}

/// return "John" from the full name "Smith, John A."
fn first_name(line: &str) -> Option<&str> {
    let s = name(line);
    // println!("detecting first name in {}", s);
    let mut f1 = s.split(", ");
    f1.next();
    let prospect = f1.next();
    if prospect == None {
        return None;
    }
    let s2 = prospect.unwrap();
    let mut f2 = s2.split(" ");
    let s4 = f2.next().unwrap();
    // println!("found {}", s4);
    Some(s4)
}

fn most_common_first_name(data: &str) -> Vec<String> {
    let mut max = 0;
    let mut counts = HashMap::new();
    for line in data.lines() {
        let prospect = first_name(line);
        if prospect == None {
            continue;
        }
        let key = prospect.unwrap();
        let cnt = counts.entry(key).or_insert(0);
        *cnt += 1;
        if *cnt > max {
            max = *cnt;
        }
    }

    // search max
    let mut s = "";
    for (key, cnt) in counts {
        if cnt == max {
            s = key;
            break;
        }
    }
    vec![format!(
        "The most common first name is: {} and it occurs: {} times.",
        s, max
    )]
}

#[test]
fn test_first_name() {
    let want = "JOHN";
    let got = first_name(
        "C00629618|N|TER|P|201701230300133512|15C|IND|\
         PEREZ, JOHN A|LOS ANGELES|CA|90017|",
    )
    .unwrap();
    assert_eq!(want, got);
}
